#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		unsigned int sceneNum = boost::any_cast<unsigned>(m_TransientDataCollection.find("SceneNumber")->second);

		if(sceneNum > 1)
			QObject::connect(backButton, SIGNAL(released()), this, SLOT(mf_backButton()));
		if (sceneNum < 1000)
			QObject::connect(forwardButton, SIGNAL(released()), this, SLOT(mf_forwardButton()));

		char buff[10];
		std::string label = "Scene ";
		label.append(itoa(sceneNum, buff, 10));
		sceneNumber->setText(label.c_str());
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		if (m_uMainWindow->objectName().isEmpty())
			m_uMainWindow->setObjectName(QStringLiteral("MainWindow"));
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 4, 3, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 0, 3, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 3, 5, 1, 1);

		forwardButton = new QPushButton(centralWidget);
		forwardButton->setObjectName(QStringLiteral("forwardButton"));

		gridLayout->addWidget(forwardButton, 3, 4, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 0, 2, 1, 1);

		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));

		gridLayout->addWidget(backButton, 3, 3, 1, 1);

		sceneNumber = new QLabel(centralWidget);
		sceneNumber->setObjectName(QStringLiteral("sceneNumber"));
		sceneNumber->setAlignment(Qt::AlignCenter);

		gridLayout->addWidget(sceneNumber, 2, 3, 1, 2);

		m_uMainWindow->setCentralWidget(centralWidget);

		forwardButton->setText(QApplication::translate("MainWindow", "Go forward", Q_NULLPTR));
		backButton->setText(QApplication::translate("MainWindow", "Go back", Q_NULLPTR));
		sceneNumber->setText(QApplication::translate("MainWindow", "Scene 1", Q_NULLPTR));
	}

	void InitialScene::mf_forwardButton()
	{
		unsigned int sceneNumber = boost::any_cast<unsigned>(m_TransientDataCollection.find("SceneNumber")->second);
		sceneNumber++;

		m_TransientDataCollection.erase("SceneNumber");
		m_TransientDataCollection.emplace("SceneNumber",sceneNumber);

		char buff[10];
		const std::string c_szNextSceneName = itoa(sceneNumber,buff,10);
		emit SceneChange(c_szNextSceneName);
	}

	void InitialScene::mf_backButton()
	{	
		unsigned int sceneNumber = boost::any_cast<unsigned>(m_TransientDataCollection.find("SceneNumber")->second);
		sceneNumber--;

		m_TransientDataCollection.erase("SceneNumber");
		m_TransientDataCollection.emplace("SceneNumber", sceneNumber);

		char buff[10];
		const std::string c_szNextSceneName = itoa(sceneNumber, buff, 10);
		emit SceneChange(c_szNextSceneName);
	}
}

