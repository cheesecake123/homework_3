#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>

const std::string sceneName = "1";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{
	}

	void ApplicationModel::defineScene()
	{
		for (int i = 1; i <= 1000; i++)
		{
			char buff[10];
			IScene* scene = new InitialScene(itoa(i,buff,10));
			m_Scenes.emplace(buff, scene);
		}
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = sceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		unsigned int sceneNumber = 1;
		m_TransientData.emplace("SceneNumber", sceneNumber);
	}
}
