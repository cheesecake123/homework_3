#pragma once
#include <spring\Framework\IScene.h>
#include <qgridlayout.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qlabel>
#include <qlineedit.h>
#include <qobject.h>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~InitialScene();

	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QSpacerItem *verticalSpacer_2;
		QSpacerItem *verticalSpacer;
		QSpacerItem *horizontalSpacer_2;
		QPushButton *forwardButton;
		QSpacerItem *horizontalSpacer;
		QPushButton *backButton;
		QLabel *sceneNumber;

		private slots:
		void mf_backButton();
		void mf_forwardButton();

	};
}
